let dogsFavor = [{
    name: 'Мария',
    lastName: 'Салтыкова',
    age: 25
}, {
    name: 'Осана',
    lastName: 'Меньшинова',
    age: 20
}, {
    name: 'Андрей',
    lastName: 'Первозваный',
    age: 100
}, {
    name: 'Василий',
    lastName: 'Гофман',
    age: 40
}, {
    name: 'Поручик',
    lastName: 'Ржевский',
    age: 'вечно молодой'
}];

let catsFavor = [{
    name: 'Мария',
    lastName: 'Розгозина',
    age: 22
}, {
    name: 'Осана',
    lastName: 'Меньшинова',
    age: 20
}, {
    name: 'Андрей',
    lastName: 'Первозваный',
    age: 100
}, {
    name: 'Алексей',
    lastName: 'Гофман',
    age: 40
}, {
    name: 'Капитан',
    lastName: 'Очевидность',
    age: 'вечно молодой'
}];



function filterSect(dogs, cats) {
    for (let i = 0; i < dogs.length; i++) {
        for (let j = 0; j < cats.length; j++) {
            if (JSON.stringify(dogs[i]) === JSON.stringify(cats[j])) {
                dogs.splice(i, 1)
            }
        }
    }

    return dogs
}


const peopleInHeaven = filterSect(dogsFavor, catsFavor);
