const cashField = document.getElementById('money-sum');
const pinField = document.getElementById('pin-code');
const submitBtn = document.getElementById('get-cash');
const result = document.getElementById('cash-request-result');

const answers = {
    blocked: 'Ваша карта заблокирована, обратитесь в банк для ее разблокировки',
    invalidPin: 'Неправильный пин-код! Попробуйте пожалуйста снова!',
    attemptsOver: 'Неправильный пин-код! Вы исчерпали количество попыток. Ваша карта заблокирована, обратитесь в' +
        ' банк для ее разблокировки',
    ok: 'Получите ваши ',
    notEnoughMoney: 'К сожалению, на вашем счету недостаточно средств'
};

const creditCard = {
    cashValue: 10000,
    pinCode: '7589',
    attempts: 3,
    status: 'active',

    getCash: function (pin, value) {
        if (this.status === 'disabled') {
            return answers.blocked

        } else if (pin !== this.pinCode && this.attempts > 0) {
            this.attempts--;
            return answers.invalidPin

        } else if (this.attempts === 0) {
            this.status = 'disabled';
            return answers.attemptsOver

        } else if (this.cashValue >= value) {
            this.cashValue -= value;
            this.attempts = 3;
            return answers.ok + value + ' USD'

        } else {
            this.attempts = 3;
            return answers.notEnoughMoney
        }
    }
};

submitBtn.addEventListener('click', () => {
    result.innerText = creditCard.getCash(pinField.value, +cashField.value)
});

