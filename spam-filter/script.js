const wordCount = 3;

function isSpam(message, spamWord, wordCount) {
    return message.split(' ').filter(word => word.toLowerCase().indexOf(spamWord.toLowerCase()) !== -1).length >= wordCount
}

$('#send-comment').click(() => {
    $('#spam-check-result').text(isSpam($('#comment').val(), $('#spam-word').val(), wordCount));
})

